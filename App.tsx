/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */



import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import MyContacts from './screen/Contacts';
import Navigation from './screen/navigation';
import Scan from './screen/Scan';

const App = () => {
  const [isContact, setContact] = useState(true);

  const onButttonPress = () => {
    setContact(!isContact);
  };

  const buttonView = () => {
    return (
      <View
        style={{
          backgroundColor: '#999966',
          alignSelf: 'center',
          padding: '5%',
        }}>
        <TouchableOpacity onPress={onButttonPress}>
          <Text>Show {isContact ? 'QR Scanner' : 'Contact List'}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{flex: 1}}>
      {/* <View
        style={{
          flex: 1,
          flexDirection: 'row',
          marginHorizontal: '10%',
          justifyContent: 'space-between',
          marginVertical: '2%',
        }}>
        {buttonView()}
      </View>
      <View style={{flex: 10}}>{isContact ? <MyContacts /> : <Scan />}</View> */}
      {/* <Scan /> */}
      <Navigation />
    </View>
  );
};

export default App;

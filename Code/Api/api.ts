import axios from 'axios';

const getRequest = async () => {
  try {
    const resp = await axios.get('https://jsonplaceholder.typicode.com/posts');
    console.log('resp', resp);

    return resp;
  } catch (err) {
    console.error(err);
  }
};

const newPost = {
    userId: 1,
    title: 'A new post',
    body: 'This is the body of the new post'
};

const postRequest = async (url:string, body:any) => {
    try {
       console.log("body",body);
       console.log("url",url);

       
        const resp = await axios.post(url, body);
        console.log(resp.data);
        return resp
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};


export {getRequest, postRequest};

import React from 'react';
import {View, Text, ImageBackground, Image} from 'react-native';
import {images} from '../constants';

const LandingPage = (props: any) => {
  return (
    <View style={{flex: 1}}>
      <Image
        style={{flex: 1, width: '100%', height: '100%'}}
        source={images.landingImg}
        resizeMode="stretch"
      />
    </View>
  );
};

export default LandingPage;

import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MyContacts from './Contacts';
import Scan from './Scan';
import LandingPage from './LandingPage';
import Login from './Login';
import Payment from './Payment';
import PaymentSuccess from './PaymentSuccess';
import Home from './Home';
import Error from './Error';

const Stack = createNativeStackNavigator();

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="login">
        <Stack.Screen
          name="contact"
          component={MyContacts}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="scan"
          component={Scan}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="landingPage"
          component={LandingPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="payment"
          component={Payment}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="paymentSuccess"
          component={PaymentSuccess}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="home"
          component={Home}
          options={{headerShown: false}}
        />
          <Stack.Screen
          name="error"
          component={Error}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default Navigation;

import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import {postRequest} from '../Code/Api/api';

const Payment = (props: any) => {
  const {navigation} = props;

  const [payment, setPayment] = useState({} as any);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setCrm();
    return () => {
      payment;
    };
  }, []);

  const setCrm = async () => {
    const jsonValue = await AsyncStorage.getItem('CRN');
    jsonValue != null ? JSON.parse(jsonValue) : null;
    payment['CRN'] = jsonValue;
  };

  const goBack = () => {
    navigation.navigate('scan');
  };

  const onTextChange = (key: string, val: any) => {
    const data: any = {...payment};
    data[key] = val;
    data['Remark'] = 'Good';
    setPayment(data);
  };

  const sendPayment = async () => {
    setLoading(true);

    const body = {
      ...payment,
    };
    const res = await postRequest(
      'https://netbank.uat.kotak.com/DotNetApps/CRNAPI/api/AddCRNDetails',
      body,
    );
    if (res?.status == 200) {
      setLoading(false);
      navigation.navigate('paymentSuccess');
    } else {
      navigation.navigate('error');
    }
  };

  return (
    <View style={{flex: 1, marginHorizontal: '2%', marginVertical: '5%'}}>
      <View style={{flex: 2}} />
      <View style={{flex: 6}}>
        <View style={{flex: 1, alignItems: 'center', marginTop: '5%'}}>
          <Text style={{fontSize: 25}}>Enter Amount</Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignContent: 'center',
            marginHorizontal: '10%',
          }}>
          <Icon style={{flex: 2}} name="facebook" size={40} color="#000" />
          <TextInput
            style={styles.input}
            keyboardType="numeric"
            placeholderTextColor="#fff"
            autoCapitalize="none"
            onChangeText={e => onTextChange('Amount', e)}
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'space-between',
            flexDirection: 'row',
            marginHorizontal: '10%',
          }}>
          <TouchableOpacity
            style={styles.submitButton}
            onPress={() => goBack()}>
            <Text style={{color: '#fff'}}>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.submitButton}
            onPress={() => sendPayment()}>
            <Text style={{color: '#fff'}}>Send</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        {isLoading ? (
          <Text style={{flex: 1, fontSize: 18}}>Loading...</Text>
        ) : null}
      </View>
    </View>
  );
};

export default Payment;

const styles = StyleSheet.create({
  submitButton: {
    backgroundColor: '#143269',
    padding: 10,
    width: '40%',
    alignItems: 'center',
    borderRadius: 10,
    height: 40,
  },
  input: {
    flex: 3,
    height: 40,
    borderBottomWidth: 1,
    marginRight: '20%',
    borderBottomColor: '#000',
    color: '#000',
  },
});

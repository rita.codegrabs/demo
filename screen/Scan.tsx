import React, {Component, Fragment, createRef} from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './scanStyle';
import {TouchableOpacity, Text, StatusBar, Linking, View} from 'react-native';

interface IProps {
  navigation?: any;
}

interface IState {
  scan?: boolean;
  ScanResult?: boolean;
  result: any;
}

class Scan extends Component<IProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      scan: false,
      ScanResult: false,
      result: null,
    };
  }

  private scanner = createRef<null>();

  onSuccess = (e: any) => {
    const check = e.data.substring(0, 4);
    console.log('scanned data' + check);
    this.setState({
      result: e,
      scan: false,
      ScanResult: true,
    });
    if (check === 'http') {
      Linking.openURL(e.data).catch(err =>
        console.error('An error occured', err),
      );
    } else {
      this.setState({
        result: e,
        scan: false,
        ScanResult: true,
      });
    }
  };

  activeQR = () => {
    this.setState({
      scan: true,
    });
  };
  scanAgain = () => {
    this.setState({
      scan: true,
      ScanResult: false,
    });
  };

  onButtonPress = () => {
    console.log('scanner', this.scanner);
  };

  onPayment = () => {
    this.props.navigation.navigate('payment');
  };

  render() {
    const {scan, ScanResult, result} = {...this.state};
    const desccription = 'QR code ';
    return (
      <View style={styles.scrollViewStyle}>
        <Fragment>
          <StatusBar barStyle="dark-content" />
          <Text style={styles.textTitle}>QR Code Scanner !</Text>
          {!scan && !ScanResult && (
            <View style={styles.cardView}>
              <Text numberOfLines={8} style={styles.descText}>
                {desccription}
              </Text>

              <TouchableOpacity
                onPress={this.activeQR}
                style={styles.buttonTouchable}>
                <Text style={styles.buttonTextStyle}>Click to Scan !</Text>
              </TouchableOpacity>
            </View>
          )}
          {ScanResult && (
            <Fragment>
              <Text style={styles.textTitle1}>Result !</Text>
              <View style={ScanResult ? styles.scanCardView : styles.cardView}>
                <Text>Type : {result.type}</Text>
                <Text>Result : {result.data}</Text>
                <Text numberOfLines={1}>RawData: {result.rawData}</Text>
                <View style={{alignContent: 'space-around'}}>
                  <View>
                    <TouchableOpacity
                      onPress={this.scanAgain}
                      style={styles.buttonTouchable}>
                      <Text style={styles.buttonTextStyle}>
                        Click to Scan again!
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <TouchableOpacity
                      onPress={this.onPayment}
                      style={styles.buttonTouchable}>
                      <Text style={styles.buttonTextStyle}>Go To Payment</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Fragment>
          )}
          {scan && (
            <QRCodeScanner
              reactivate={true}
              showMarker={true}
              ref={(node: any) => {
                this.scanner = node;
              }}
              onRead={this.onSuccess}
              topContent={
                <Text style={styles.centerText}>
                  Go to{' '}
                  <Text style={styles.textBold}>
                    wikipedia.org/wiki/QR_code
                  </Text>{' '}
                  on your computer and scan the QR code to test.
                </Text>
              }
              bottomContent={
                <View style={{marginBottom: '15%'}}>
                  <TouchableOpacity
                    style={styles.buttonTouchable}
                    onPress={() => {
                      this.onButtonPress();
                    }}>
                    <Text style={styles.buttonTextStyle}>OK. Got it!</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.buttonTouchable}
                    onPress={() => this.setState({scan: false})}>
                    <Text style={styles.buttonTextStyle}>Stop Scan</Text>
                  </TouchableOpacity>
                </View>
              }
            />
          )}
        </Fragment>
      </View>
    );
  }
}

export default Scan;

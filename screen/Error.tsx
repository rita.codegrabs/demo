import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const Error = (props: any) => {
  const {navigation} = props;

  const goBack = () => {
    navigation.goBack();
  };
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}} />
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{flex: 1, fontSize: 20}}>
          Something Went Wrong Try Again
        </Text>
        <View style={{flex: 1}}>
          <TouchableOpacity style={styles.submitButton} onPress={goBack}>
            <Text style={{color: '#fff'}}>Go Back</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{flex: 1}} />
    </View>
  );
};

export default Error;

const styles = StyleSheet.create({
  submitButton: {
    backgroundColor: '#143269',
    padding: 10,
    width: '70%',
    // margin: 15,
    alignItems: 'center',
    borderRadius: 10,
    height: 40,
  },
});

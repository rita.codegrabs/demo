import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const Home = (props: any) => {
  const {navigation} = props;

  const goToPaymentAction = (screen: string) => {
    navigation.navigate(screen);
  };

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}} />
      <View style={{flex: 2, alignItems: 'center'}}>
        <Text style={{flex: 1, fontSize: 25}}>Transfer Money </Text>
        <View style={{flex: 2, flexDirection: 'row'}}>
          <View style={{paddingHorizontal: '5%'}}>
            <TouchableOpacity
              onPress={() => {
                goToPaymentAction('contact');
              }}>
              <Icon name="id-badge" size={80} color="#808080" />
              <Text style={{fontSize: 14, marginTop: '10%'}}>Contact List</Text>
            </TouchableOpacity>
          </View>
          <View style={{paddingHorizontal: '5%'}}>
            <TouchableOpacity
              onPress={() => {
                goToPaymentAction('scan');
              }}>
              <Icon name="qrcode" size={80} color="#808080" />
              <Text style={{fontSize: 14, marginTop: '10%'}}>Scan Code</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{flex: 1}} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});

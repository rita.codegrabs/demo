import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {postRequest} from '../Code/Api/api';
import {images} from '../constants';

const Login = (props: any) => {
  const {navigation} = props;

  const [login, setLogin] = useState({} as any);
  const [isLoading, setLoading] = useState(false);

  const onTextChange = (key: string, val: any) => {
    const data: any = {...login};
    data[key] = val;
    setLogin(data);
  };

  const submit = async () => {
    setLoading(true);
    const body = {
      ...login,
    };
    const res = await postRequest(
      'https://netbank.uat.kotak.com/DotNetApps/CRNAPI/api/ValidateUsers',
      body,
    );
    if (res?.status === 200) {
      await AsyncStorage.setItem('CRN', '30001287');
      setLoading(false);
      navigation.navigate('home');
    } else {
      navigation.navigate('error');
    }
  };

  return (
    <View style={styles.container}>
      <View style={{flex: 0.1}} />
      <View style={{flex: 0.8}}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Image
            style={{flex: 1, width: '70%', height: '50%'}}
            source={{uri: images.kotak_logo}}
            resizeMode="contain"
          />
        </View>
        <View style={{flex: 2}}>
          <View style={{flex: 1}}>
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Enter username"
              placeholderTextColor="#fff"
              autoCapitalize="none"
              onChangeText={e => onTextChange('CRN', e)}
            />
          </View>
          <View style={{flex: 1}}>
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Enter Password"
              placeholderTextColor="#fff"
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={e => onTextChange('Password', e)}
            />
          </View>
          <View style={{flex: 1, alignItems: 'center', paddingTop: '10%'}}>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => submit()}>
              <Text style={{color: '#fff'}}>Submit</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex: 1}}>
            {isLoading ? (
              <Text style={{flex: 1, fontSize: 18}}>Loading...</Text>
            ) : null}
          </View>
        </View>
      </View>
      <View style={{flex: 0.1}} />
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
    paddingHorizontal: '10%',
  },
  input: {
    height: 40,
    borderBottomWidth: 1,
    padding: 10,
    borderBottomColor: '#fff',
    color: '#fff',
  },
  submitButton: {
    backgroundColor: '#143269',
    padding: 10,
    width: '50%',
    alignItems: 'center',
    borderRadius: 10,
    height: 40,
  },
});

import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const PaymentSuccess = (props: any) => {
  const {navigation} = props;

  const onHomeScreen = () => {
    navigation.navigate('home');
  };

  return (
    <View style={{flex: 1}}>
      <View style={{flex: 2}} />
      <View style={{flex: 3}}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Icon name="check-circle" size={80} color="#118c42" />
          <Text style={{marginTop: '10%', color: '#118c42', fontSize: 25}}>
            Payment Successful
          </Text>
        </View>
      </View>
      <View style={{flex: 1, alignItems: 'center'}}>
        <TouchableOpacity style={styles.goTOHome} onPress={onHomeScreen}>
          <Text style={{color: '#fff', fontSize: 15}}>Go To Home Screen</Text>
        </TouchableOpacity>
      </View>
      <View style={{flex: 1}} />
    </View>
  );
};

export default PaymentSuccess;

const styles = StyleSheet.create({
  goTOHome: {
    backgroundColor: '#143269',
    padding: 10,
    width: '40%',
    // margin: 15,
    alignItems: 'center',
    borderRadius: 10,
    height: 40,
  },
});

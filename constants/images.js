const landingImg = require("../assets/images/landing_page.jpeg");
const kotak_logo= 'https://www.kotak.com/content/dam/Kotak/kotak-logo.png'
const ruppe =require("../assets/images/rupee1.jpg");
const error = require("../assets/images/error.jpg")

export default {
    landingImg,
    kotak_logo,
    ruppe,
    error
}